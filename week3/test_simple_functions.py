from simple_functions import is_adult, return_words_containing_letter_a


def test_is_adult_for_age_greater_than_18():
    assert is_adult(19)


def test_is_adult_for_age_equal_18():
    assert is_adult(18)


def test_is_adult_for_age_less_than_18():
    assert not is_adult(17)


def test_list_containing_all_words_with_letter_a():
    input_list = ["Parrot", "Cat", "Any", "Amazonia", "Papaya"]
    expected_list = ["Parrot", "Cat", "Any", "Amazonia", "Papaya"]
    assert return_words_containing_letter_a(input_list) == expected_list


def test_list_containing_mixed_words():
    input_list = ["Parrot", "Bye", "Any", "Amazonia", "Papaya", "Miro", "why", "alabama"]
    expected_list = ["Parrot", "Any", "Amazonia", "Papaya", "alabama"]
    assert return_words_containing_letter_a(input_list) == expected_list


def test_list_not_containing_words_with_a():
    input_list = ["Bye", "Miro", "why"]
    expected_list = []
    assert return_words_containing_letter_a(input_list) == expected_list


def test_empty_list():
    assert return_words_containing_letter_a([]) == []
