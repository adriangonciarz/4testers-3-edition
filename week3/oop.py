class Dog:
    def __init__(self, name):
        self.name = name
        self.food_in_belly = 0

    def say_hello(self):
        print(f"Hau hau, my name is {self.name}")

    def feed(self, amount_of_food):
        self.food_in_belly += amount_of_food


if __name__ == '__main__':
    dog1 = Dog("Azor")
    dog2 = Dog("Burek")
    print(dog1.name)
    print(dog2.name)
    dog1.say_hello()
    dog2.say_hello()

    dog1.feed(100)

    print(dog1.food_in_belly)
    print(dog2.food_in_belly)
