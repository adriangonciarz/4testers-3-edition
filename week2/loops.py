def print_numbers_from_0_to_limit(limiting_number):
    for i in range(limiting_number + 1):
        print(i)


def print_numbers_divisible_by_7(from_number, to_number):
    for i in range(from_number, to_number + 1):
        if i % 7 == 0:
            print(f"Number {i} is divisible by 7")


def temperature_in_celsius_to_fahrenheit(temp_celsius):
    return 9 / 5 * temp_celsius + 32


def print_each_temperature_in_celsius_and_fahrenheit(temperatures_in_celsius_list):
    for temp_celsius in temperatures_in_celsius_list:
        converted_temp = round(temperature_in_celsius_to_fahrenheit(temp_celsius), 2)
        print(f"Temperature in Celsius: {temp_celsius}; temperature in Fahrenheit: {converted_temp}")


if __name__ == '__main__':
    print_numbers_from_0_to_limit(20)
    print_numbers_divisible_by_7(0, 35)

    spring_temperatures = [10.3, 23.4, 15.8, 19.0, 14.0, 23.0, 25.0]
    print_each_temperature_in_celsius_and_fahrenheit(spring_temperatures)
