def check_if_standard_conditions(temp_in_celsius, pressure_in_hpa):
    if temp_in_celsius == 0 and pressure_in_hpa == 1013:
        return True
    else:
        return False


def get_grade_for_test_percentage(test_percentage):
    if test_percentage >= 90:
        return 5
    elif test_percentage >= 75:
        return 4
    elif test_percentage >= 50:
        return 3
    else:
        return 2


def print_promotions_for_age(age):
    if age >= 65:
        print(f"for age {age} you have 70% off")
    elif age >= 50:
        print(f"for age {age} you have 50% off")
    elif age >= 38:
        print(f"for age {age} you have 25% off")
    else:
        print(f"for age {age} you don't have any promo")


if __name__ == '__main__':
    print(f"Checking if standard conditions for 0 Celsius, 1013 hPa: {check_if_standard_conditions(0, 1013)}")
    print(f"Checking if standard conditions for 0 Celsius, 1014 hPa: {check_if_standard_conditions(0, 1014)}")
    print(f"Checking if standard conditions for 1 Celsius, 1013 hPa: {check_if_standard_conditions(1, 1013)}")
    print(f"Checking if standard conditions for 1 Celsius, 1013 hPa: {check_if_standard_conditions(1, 1014)}")

    print(f"Grade for 99% is {get_grade_for_test_percentage(99)}")
    print(f"Grade for 99% is {get_grade_for_test_percentage(90)}")
    print(f"Grade for 99% is {get_grade_for_test_percentage(89)}")

    print_promotions_for_age(66)
    print_promotions_for_age(65)
    print_promotions_for_age(64)
    print_promotions_for_age(51)
    print_promotions_for_age(50)
    print_promotions_for_age(43)
    print_promotions_for_age(35)