def get_words_reversed(list_of_words):
    output_list = []

    for word in list_of_words:
        output_list.append(word[::-1])

    return output_list


def map_numbers_to_squared(input_list):
    return [number ** 2 for number in input_list]


def get_words_reversed_easier(list_of_words):
    return [word[::-1] for word in list_of_words]


def get_words_containing_letter_a(list_of_words):
    return [word for word in list_of_words if "A" in word.upper()]


if __name__ == '__main__':
    print(get_words_reversed(["Horse", "sky", "football", "dreams"]))
    print(get_words_reversed_easier(["Horse", "sky", "football", "dreams"]))

    print(get_words_containing_letter_a(["Admin", "admires", "Piotr", "LA"]))
