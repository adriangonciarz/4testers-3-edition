import random
import string


def get_random_string(length):
    # choose from all lowercase letter
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(length))
    print("Random string of length", length, "is:", result_str)
    return result_str


def get_random_login_data_for_email(email):
    return {
        "email": email,
        "password": get_random_string(12)
    }


if __name__ == '__main__':
    my_friend_dictionary = {
        "name": "Tomasz",
        "age": 29,
        "hobbies": ["hitchhiking", "sport"],
    }

    my_friend_dictionary["name"] = "Bartosz"
    my_friend_dictionary["age"] = my_friend_dictionary["age"] + 1
    my_friend_dictionary["age"] += 2
    my_friend_dictionary["hobbies"].append("archery")

    print(my_friend_dictionary)

    new_login_data = get_random_login_data_for_email("adrian@example.com")
    print(new_login_data)