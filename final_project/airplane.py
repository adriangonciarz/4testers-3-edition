class Airplane:
    def __init__(self, airplane_name, airplane_seats):
        self.flight_distance = 0
        self.occupied_seats = 0
        self.airplane_seats = airplane_seats
        self.airplane_name = airplane_name

    def fly(self, distance):
        self.flight_distance += distance

    def is_service_required(self):
        if self.flight_distance > 10000:
            return True
        else:
            return False

    def board_passengers(self, number_of_passengers):
        if number_of_passengers > self.get_available_seats():
            self.occupied_seats = self.airplane_seats
        else:
            self.occupied_seats += number_of_passengers

    def get_available_seats(self):
        return self.airplane_seats - self.occupied_seats


if __name__ == '__main__':
    airplane1 = Airplane("Boeing", 200)
    airplane2 = Airplane("Airbus", 250)

    airplane1.fly(1000)
    airplane2.fly(2000)

    airplane1.board_passengers(50)

    print(airplane1.occupied_seats)
    print(airplane1.get_available_seats())
    print(airplane1.flight_distance)

    print(airplane1.is_service_required())
    print(airplane2.is_service_required())
