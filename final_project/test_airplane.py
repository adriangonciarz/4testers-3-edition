from airplane import Airplane


def test_flight_distance_and_occupied_seats():
    test_airplane = Airplane("Boeing", 200)
    assert test_airplane.flight_distance == 0
    assert test_airplane.occupied_seats == 0


def test_check_if_flight_distance_if_correct():
    test_airplane = Airplane("Boeing", 200)
    test_airplane.fly(5000)
    assert test_airplane.flight_distance == 5000


def test_check_if_flight_distance_if_correct_after_second_flight():
    test_airplane = Airplane("Boeing", 200)
    test_airplane.fly(5000)
    test_airplane.fly(3000)
    assert test_airplane.flight_distance == 8000


def test_check_if_airplane_doesnt_need_service():
    test_airplane = Airplane("Boeing", 200)
    test_airplane.fly(9999)
    assert not test_airplane.is_service_required()


def test_check_if_airplane_needs_service():
    test_airplane = Airplane("Boeing", 200)
    test_airplane.fly(10001)
    assert test_airplane.is_service_required()


def test_available_seats_in_airplane():
    test_airplane = Airplane("Boeing", 200)
    test_airplane.board_passengers(180)
    assert test_airplane.get_available_seats() == 20
    assert test_airplane.occupied_seats == 180


def test_boarding_over_max_available_seats_in_airplane():
    test_airplane = Airplane("Boeing", 200)
    test_airplane.board_passengers(201)
    assert test_airplane.get_available_seats() == 0
    assert test_airplane.occupied_seats == 200
