def get_middle_element(input_list):
    return input_list[len(input_list) // 2 + (len(input_list) % 2 - 1)]
    #                           3          +            1         - 1


def calculate_revenue_in_moth_from_to(list_of_daily_revenues, start_day_number, end_day_number):
    index_start = start_day_number - 1
    index_end = end_day_number
    sub_month_revenue = list_of_daily_revenues[index_start:index_end]
    print(f"Revenues for month since day {start_day_number} to day {end_day_number} are {sub_month_revenue}")
    return sum(sub_month_revenue)


if __name__ == '__main__':
    # len() // 2
    input_odd_1 = [1, 4, 7, 19, 33]
    input_odd_2 = [1, 4, 7, 19, 33, 99, 156]

    # len() // 2 - 1
    input_even_1 = [1, 4, 7, 19, 33, 78]
    input_even_2 = [1, 4, 7, 9, 33, 78, 100, 120]

    print(get_middle_element(input_odd_1))  # 7
    print(get_middle_element(input_odd_2))  # 19
    print(get_middle_element(input_even_1))  # 7
    print(get_middle_element(input_even_2))  # 9

    december_revenue = [100, 230, 560, 330, 450, 800, 120, 150, 900, 770]
    first_week_revenue = december_revenue[0:3]
    print(f"First week of December revenue: {first_week_revenue}")

    """Napisz funkcję, która przyjmie listę utargów w miesiącu, dzień startu i dzień końca i zwróci sumę utargu od startu do końca"""
    november_revenue = [101, 231, 561, 331, 451, 801, 121, 100, 230, 560, 330, 450, 800, 120, 100, 230, 560, 330, 450,
                        800, 120, 100, 230, 560, 330, 450, 800, 120, 888, 999]
    print(f"Number of november days: {len(november_revenue)}")
    sum_from_2_to_5_of_november = calculate_revenue_in_moth_from_to(november_revenue, 2, 5)
    print(f"Revenue from 2 to 5 of November is: {sum_from_2_to_5_of_november}")

    """Napisz funkcję, która przyjmię listę utargów w miesiącu i zwróci średni dzienny utarg"""
