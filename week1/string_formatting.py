def city_greeter(name, city):
    greeting = f"Witaj {name.upper()}! Miło Cię widzieć w naszym mieście: {city.upper()}!"
    print(greeting)


if __name__ == '__main__':
    first_name = "Adrian"
    last_name = "Gonciarz"
    email = first_name.lower() + "." + last_name.lower() + "@4testers.pl"
    print(email)

    email_formatted = f"{first_name.lower()}.{last_name.lower()}@4testers.pl"
    print(email_formatted)

    city_greeter("Michał", "Toruń")
    city_greeter("Beata", "Gdynia")
